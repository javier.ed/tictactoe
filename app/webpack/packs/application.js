/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb
import { Application } from "stimulus"
import { definitionsFromContext } from 'stimulus/webpack-helpers'

const application = Application.start()
const context = require.context('../controllers', true, /.js$/)
application.load(definitionsFromContext(context))

var resizeTable = () => {
  var current_height = (window.innerHeight > 0) ? window.innerHeight : screen.height
  var current_width = (window.innerWidth > 0) ? window.innerWidth : screen.width
  if(current_height < current_width){
    document.getElementById('table').style.height = '100%'
    document.getElementById('table').style.width = current_height + 'px'
  }
  else{
    document.getElementById('table').style.width = '100%'
    document.getElementById('table').style.height = current_height + 'px'
  }
}

window.onload = resizeTable

window.onresize = resizeTable
